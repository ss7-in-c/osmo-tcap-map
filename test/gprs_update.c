/* Early tests with libasn1c and libosmo-asn1-tcap + libosmo-asn1-map */

/* (C) 2010 by Harald Welte <laforge@gnumonks.org>
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include <osmocore/utils.h>

#include <asn_application.h>

/* TCAP */
#include <TCMessage.h>
#include <DialoguePDU.h>
#include <ExternalPDU.h>

/* MAP */
#include <Invoke.h>
#include <InvokeIdType.h>
#include <GSMMAPOperationLocalvalue.h>
#include <UpdateGprsLocationArg.h>

static const int map_app_ctx_oid[] = { 0, 4, 0, 0, 1, 0, 0, 0 };
static const int gprsLocationUpdateContext_v3[] = { 0, 4, 0, 0, 1, 0, 32, 3 };

static const int diag_ref_struct[] = { 0, 0, 17, 773, 1, 1, 1 };
static const int diag_ref_unstruct[] = { 0, 0, 17, 773, 1, 2, 1 };

/* construct MAP invoke (component) */
static Component_t *create_map_invoke(uint32_t opcode, asn_TYPE_descriptor_t *td,
				      void *sptr)
{
	Component_t *comp;
	Invoke_t *inv;

	comp = calloc(1, sizeof(*comp));
	memset(comp, 0, sizeof(*comp));

	comp->present = Component_PR_invoke;
	inv = &comp->choice.invoke;

	inv->invokeID = 1; /* FIXME: what is this? */
	inv->opCode.present = OPERATION_PR_localValue;
	asn_long2INTEGER(&inv->opCode.choice.localValue, opcode);

	if (td && sptr)
		inv->parameter = ANY_new_fromType(td, sptr);

	return comp;
}

static Component_t *upd_gprs_loc_invoke(void)
{
	Component_t *comp;

	UpdateGprsLocationArg_t ula;
	memset(&ula, 0, sizeof(ula));
	OCTET_STRING_fromString(&ula.imsi, "012345678912345");
	OCTET_STRING_fromString(&ula.sgsn_Number, "1");
	OCTET_STRING_fromString(&ula.sgsn_Address, "1.2.3.4");

	xer_fprint(stdout, &asn_DEF_UpdateGprsLocationArg, &ula);

	/* build MAP invoke component with UpdateGPRSLocArg */
	comp = create_map_invoke(GSMMAPOperationLocalvalue_updateGprsLocation,
			  	&asn_DEF_UpdateGprsLocationArg, &ula);

	xer_fprint(stdout, &asn_DEF_Component, comp);

	return comp;
}

static uint8_t _dial_version1 = 0x80;
static BIT_STRING_t dial_version1 = {
	.buf = &_dial_version1,
	.size = 1,
	.bits_unused = 7,
};

/* construct DialoguePDU wwith AARQ */
static DialoguePDU_t *upd_gprs_loc_aarq()
{
	DialoguePDU_t *dial;
	AARQ_apdu_t *aarq;

	dial = calloc(1, sizeof(*dial));
	memset(dial, 0, sizeof(*dial));
	dial->present = DialoguePDU_PR_dialogueRequest;

	aarq = &dial->choice.dialogueRequest;
	aarq->protocol_version = &dial_version1;
	OBJECT_IDENTIFIER_set_arcs(&aarq->application_context_name,
				   &gprsLocationUpdateContext_v3,
				   sizeof(gprsLocationUpdateContext_v3[0]),
				   ARRAY_SIZE(gprsLocationUpdateContext_v3));

	xer_fprint(stdout, &asn_DEF_DialoguePDU, dial);

	return dial;
}


static int tcap_msg(void)
{
	uint32_t trans_id = 1;
	struct ComponentPortion cp;
	ExternalPDU_t ext;
	Component_t *comp;
	DialoguePDU_t *dial;
	TCMessage_t *tcm;

	memset(&ext, 0, sizeof(ext));

	tcm = calloc(1, sizeof(*tcm));
	memset(tcm, 0, sizeof(*tcm));

	tcm->present = TCMessage_PR_begin;
	OCTET_STRING_fromBuf(&tcm->choice.begin.otid,
			     (const char *) &trans_id, sizeof(trans_id));

	/* build dialogue sequence from OID and DialoguePDU */
	dial = upd_gprs_loc_aarq();

	OBJECT_IDENTIFIER_set_arcs(&ext.oid, &diag_ref_struct,
				   sizeof(diag_ref_struct[0]),
				   ARRAY_SIZE(diag_ref_struct));
	ANY_fromType(&ext.dialog, &asn_DEF_DialoguePDU, dial);
	tcm->choice.begin.dialoguePortion =
				ANY_new_fromType(&asn_DEF_ExternalPDU, &ext);

	/* add SEQUENCE_OF invoke component */
	memset(&cp, 0, sizeof(cp));
	tcm->choice.begin.components = &cp;
	comp = upd_gprs_loc_invoke();
	ASN_SEQUENCE_ADD(&cp.list, comp);

	xer_fprint(stdout, &asn_DEF_TCMessage, tcm);
}

int main(int argc, char **argv)
{
	tcap_msg();
}
